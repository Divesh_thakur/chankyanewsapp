package com.example.chanakyanewsapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.example.chanakyanewsapp.ui.BaseAppCompatActivity;
import com.example.chanakyanewsapp.ui.SignUpActivity;
import com.example.chanakyanewsapp.ui.HomePage;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseAppCompatActivity {
    @BindView(R.id.buttonLogin)
    AppCompatButton buttonLogin;

    @BindView(R.id.textViewSignUp)
    AppCompatTextView textViewSignUp;

    @BindView(R.id.editTextEmail)
    AppCompatEditText editTextEmail;

    @BindView(R.id.editTextPassword)
    AppCompatEditText editTextPassword;

    @OnClick(R.id.textViewSignUp)
    void signUp(){
        SignUpActivity.startSelf(this);
    }

    @OnClick(R.id.buttonLogin)
    void login(){
        HomePage.startSelf(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        buttonLogin = findViewById(R.id.buttonLogin);
//        buttonLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                HomePage.startSelf(MainActivity.this);
//            }
//        });
    }

    public static void startSelf(Activity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }
}