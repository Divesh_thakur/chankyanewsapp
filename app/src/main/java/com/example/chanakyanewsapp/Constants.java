package com.example.chanakyanewsapp;

public class Constants {

    public static final String API_URL = "  ";
    public static final String APP_URL = "  ";
    public static final String DEFAULT_REST_ERROR_CODE = "default_rest_error_code";
    public static final String DEFAULT_REST_NO_NET_ERROR_CODE = "default_rest_no_net_error_code";
    public static final int SPLASH_TIME_OUT = 2000;
    public static final int MIN_DATE = 1970-01-01;
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String MONTH_DATE_FORMAT="EEE MMM dd yyyy hh:mm:ss 'GMT'";
    public static final String TIME_FORMAT = "hh:mm aa";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm aa";

    //REST or SOCKET Headers
    public interface HEADERS {
        String CONTENT_TYPE = "Content-Type";
        String ACCEPT = "accept";
        String AUTHORIZATION = "authorization";

    }

    public interface DEVICE_TYPE{
        String DEVICE_TYPE="android";
    }

    public interface EVENT_VENUE_PHOTO_REFERENCE{
        String GOOGLE_URL="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=";
        String API_KEY_QUERY="&key=";
    }

    public interface CONTENT_TYPE {
        String APPLICATION_JSON = "application/json";
    }

    public interface SHARED_PREFERENCE_KEY {
        String INTRODUCTION_COMPLETE = "INTRODUCTION_COMPLETE";
        String FIRST_TIME_SCREEN_SURVEY_COMPLETE="FIRST_TIME_SCREEN_SURVEY_COMPLETE";
        String FCM_TOKEN_SUBMITTED="fcm_token_submitted";
        String ACCESS_TOKEN = "access_token";
        String PREVIOUS_EVENTS="PREVIOUS_EVENTS";
        String PERSIST_EVENT_LIST = "persist_event_list";
        String PERSIST_CARD_DETAILS_LIST = "persist_card_details_list";
        String PERSIST_MANUAL_EVENT_NUMBER="persist_manual_event_number";
        String SAVE_CARD_TO_PROFILE="save_card_to_profile";
    }

    public interface INTENT_KEY {
        String SELECTED_EVENT_NUMBER = "selected_event_number";
        String SELECTED_EVENT_DATE = "selected_event_date";
        String SELECTED_LOCATION="selected_location";
        String SELECTED_LATITUDE="selected_latitude";
        String SELECTED_LONGITUDE="selected_longitude";
        String SELECTED_BUDGET="selected_budget";
        String SELECTED_PEOPLE="selected_people";
        String SELECTED_START_TIME="selected_start_time";
        String SELECTED_END_TIME="selected_end_time";
        String SELECTED_TYPE="selected_type";
        String SELECTED_STYLE="selected_style";
        String SELECTED_TIME_TYPE="selected_time_type";
        String EDIT_INDEX="edit_index";
        String IS_EDITING="is_editing";
        String EDIT_PROFILE_SUCCESS ="edit_profile_success";
        String SEARCH_EVENT_VENUE="select_event_venue";
        String SELECTED_EPOCH_START_TIME="selected_start_time_epoch";
        String SELECTED_EPOCH_END_TIME="selected_end_time_epoch";
        String MANUAL_SUGGESTION="manual_suggestion";
        String EVENT_DATA="event_data";
    }

    public interface MIME {
        String IMAGE_PNG = "image/png";
    }

    public interface DEFAULT_VALUE {
        int EVENT_NUMBER = 1;
        int EVENT_NUMBER_OF_PEOPLE = 3;
        String DEFAULT_BUDGET = "<$50 - $1k+";
        String DEFAULT_BUDGET_MIN_VALUE = "<$50";
        String DEFAULT_BUDGET_MAX_VALUE = "$1k+";
        String[] BUDGET_TICK_LABELS={"<$50","$200","$400","$600","$800","$1k+"};
    }

    public interface CLIENT_ID{
        String SEAT_GEEK_CLIENT_ID = "MjEzNzY1MDV8MTYwNTc1MjczNy44NzE5MDY1";
    }
    public interface NPS_API_KEY{
        String API_KEY = "KE8DkhA2T14n3KKMEluMnnJLyBieEePWs1L9qbWD";
    }

    public interface EVENT_STYLE{
        String MOVIE = "Movie";
        String RESTAURANT = "Restaurant";
        String EDUCATIONAL = "Educational";
        String HISTORIC = "Historic";
        String ART = "Art";
        String CULTURE = "Culture";
        String CITY_ATTRACTION = "City Attractions";
        String SPORTING_EVENT = "Sporting Events";
        String BOOK_A_TOUR = "Book a Tour";
        String OUTDOORS_EXCITING = "Outdoors,Exciting";
        String ADVENTUROUS = "Adventurous";
        String SEASONAL = "Seasonal";
        String FITNESS = "Fitness";
        String SHOW = "Show";
    }

    public interface TIME_SLOTS{
        String FULL_DAY_EVENTS = "Full Day Event";
        String EVENING_EVENTS = "Evening Event";
        String DAY_TIME_EVENTS = "Daytime Event";
        String AFTER_HOURS_EVENT = "After Hours Event";
        String FULL_DAY_START_HOUR=" 7:00 AM";
        String FULL_DAY_END_HOUR=" 6:59 PM";
        String EVENING_EVENTS_START_HOUR=" 5:00 PM";
        String EVENING_HOURS_END_HOUR=" 11:59 PM";
        String DAY_TIME_START_HOUR = " 7:00 AM";
        String DAY_TIME_END_HOUR = " 4:59 PM";
        String AFTER_HOURS_START_HOUR=" 11:00 AM";
        String AFTER_HOURS_END_HOUR = " 6:00 PM";
    }
}
