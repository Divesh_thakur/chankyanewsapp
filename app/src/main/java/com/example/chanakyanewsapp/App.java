package com.example.chanakyanewsapp;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.example.chanakyanewsapp.datastore.UserDataPreference;
import com.example.chanakyanewsapp.rest.ApiFactory;

import org.greenrobot.eventbus.EventBus;

public class App extends Application {
    private static App app;
    private ApiFactory apiFactory;
    private ApiFactory apiFactoryNoAccessToken;
    private EventBus eventBus;
    private SharedPreferences defaultAppSharedPreferences;
    private UserDataPreference userDataPreference;



    public static App get() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        initBasics();
    }

    private void initBasics() {
        _initDefaultPreferences();
        _initApiFactory();
        _initEventBus();
    }

    private void _initApiFactory() {
        apiFactory = new ApiFactory();
        apiFactoryNoAccessToken = new ApiFactory(false);
        String storedToken = getDefaultAppSharedPreferences().getString(Constants.SHARED_PREFERENCE_KEY.ACCESS_TOKEN, "");
        ApiFactory.setToken(storedToken);
    }

    public ApiFactory getApiFactory() {
        return apiFactory;
    }

    public ApiFactory getApiFactoryNoAccessToken() {
        return apiFactoryNoAccessToken;
    }

    private void _initEventBus() {
        this.eventBus = EventBus.builder()
                .logNoSubscriberMessages(false)
                .sendNoSubscriberEvent(false).build();
    }

    public EventBus getEventBus() {
        return eventBus;
    }

    private void _initDefaultPreferences() {
        defaultAppSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public SharedPreferences getDefaultAppSharedPreferences() {
        return defaultAppSharedPreferences;
    }

    public SharedPreferences.Editor getDefaultSharedPreferenceEditor() {
        return getDefaultAppSharedPreferences().edit();
    }

    public String getAccessToken(){
        return getDefaultAppSharedPreferences().getString(Constants.SHARED_PREFERENCE_KEY.ACCESS_TOKEN, "");
    }

    public void updateAccessToken(String accessToken) {
        getDefaultSharedPreferenceEditor().putString(Constants.SHARED_PREFERENCE_KEY.ACCESS_TOKEN, accessToken).apply();
        ApiFactory.setToken(accessToken);
    }

    public UserDataPreference getUserDataPreference(Context context) {
        if (userDataPreference == null) {
            userDataPreference = new UserDataPreference(context);
        }
        return userDataPreference;
    }

}
