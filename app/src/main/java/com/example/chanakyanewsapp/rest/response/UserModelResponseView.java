package com.example.chanakyanewsapp.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserModelResponseView {
    @SerializedName("_id")
    public String id;

    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;

    @SerializedName("name")
    public String fullName;


    @SerializedName("phone")
    public String phone;

    public UserModelResponseView(String id, String email, String password, String fullName, String phone) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "UserModelResponseView{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
