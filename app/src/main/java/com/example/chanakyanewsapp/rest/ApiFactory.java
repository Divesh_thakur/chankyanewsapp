package com.example.chanakyanewsapp.rest;

import com.example.chanakyanewsapp.Constants;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class ApiFactory {

    private static final long TIMEOUT_SECONDS = 30;

    private final Retrofit retrofit;
    private final Retrofit retrofitForString;
    private final GsonConverterFactory gsonConverterFactory;
    private API api;
    private API apiForString;
    private static String token;


    public ApiFactory() {
        gsonConverterFactory = GsonConverterFactory.create();
        retrofit = createRetrofitInstance(true);
        retrofitForString = createRetrofitForString();
        buildApi();
    }


    public ApiFactory(boolean needAccessToken) {
        gsonConverterFactory = GsonConverterFactory.create();
        retrofit = createRetrofitInstance(needAccessToken);
        retrofitForString = createRetrofitForString();
        buildApi();
    }

    private Retrofit createRetrofitInstance(boolean needAccessToken) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        CookieHandler cookieHandler = new CookieManager();

        httpClientBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
        httpClientBuilder.readTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS);
        httpClientBuilder.connectTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS);
        httpClientBuilder.addInterceptor(new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request original = chain.request();
                Request request;
                Request.Builder requestBuilder = original.newBuilder();
                ApiFactory.this.addCommonHeaders(requestBuilder, needAccessToken);
                request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        final OkHttpClient okHttpClient;
        okHttpClient = httpClientBuilder
                .addInterceptor(logging)
                .build();

        return new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .client(okHttpClient)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    private void addCommonHeaders(Request.Builder builder, boolean needAccessToken) {
        builder.addHeader(Constants.HEADERS.ACCEPT, Constants.CONTENT_TYPE.APPLICATION_JSON);
        builder.header(Constants.HEADERS.CONTENT_TYPE, Constants.CONTENT_TYPE.APPLICATION_JSON);
        if (needAccessToken) {
            builder.header(Constants.HEADERS.AUTHORIZATION, token);
        }
    }

    private void buildApi() {
        api = retrofit.create(API.class);
        apiForString = retrofitForString.create(API.class);
    }

    public API getApi() {
        return api;
    }

    public API getApiForString() {
        return apiForString;
    }



    // methods to create retrofit instance with string response
    private Retrofit createRetrofitForString() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        final OkHttpClient okHttpClient;
        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        return new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }


    public static void setToken(String newToken) {
        token = newToken;
    }

    public static String getToken() {
        return token;
    }

}
