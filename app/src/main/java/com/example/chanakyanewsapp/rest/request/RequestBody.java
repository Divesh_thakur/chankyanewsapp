package com.example.chanakyanewsapp.rest.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RequestBody {

    public static class SignUpRequestBody{

        @SerializedName("name")
        public String name;

        @SerializedName("email")
        public String email;

        @SerializedName("password")
        public String password;

        @SerializedName("phone")
        public String phone;

        public SignUpRequestBody(String name, String email, String password, String phone) {
            this.name = name;
            this.email = email;
            this.password = password;
            this.phone = phone;
        }
    }

    public static class LoginRequestBody {
        @SerializedName("email")
        public String email;

        @SerializedName("password")
        public String password;
        public LoginRequestBody(String email, String password) {
            this.email = email;
            this.password = password;
        }
    }

    public static class ResetPasswordRequestBody {
        @SerializedName("email")
        public String email;

        public ResetPasswordRequestBody(String email) {
            this.email = email;
        }
    }

}
