package com.example.chanakyanewsapp.ui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.chanakyanewsapp.Constant;
import com.example.chanakyanewsapp.R;
import com.example.chanakyanewsapp.datastore.model.YoutubeModel;
import com.example.chanakyanewsapp.ui.adapter.NewsAdapter;
import com.example.chanakyanewsapp.ui.adapter.VideoAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoFragment extends Fragment {

    private List<YoutubeModel> youtubeModels=new ArrayList<>();

    @BindView(R.id.videoRecyclerView)
    RecyclerView videoRecyclerView;
       @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        ButterKnife.bind(this, view);
        youtubeModels.clear();
        youtubeModels.add(new YoutubeModel(Constant.videoId1));
        youtubeModels.add(new YoutubeModel(Constant.videoId2));
        youtubeModels.add(new YoutubeModel(Constant.videoId3));
        youtubeModels.add(new YoutubeModel(Constant.videoId4));
        youtubeModels.add(new YoutubeModel(Constant.videoId5));
        youtubeModels.add(new YoutubeModel(Constant.videoId6));
        youtubeModels.add(new YoutubeModel(Constant.videoId7));
        youtubeModels.add(new YoutubeModel(Constant.videoId8));
        youtubeModels.add(new YoutubeModel(Constant.videoId9));

        for (int i = 0; i < youtubeModels.size() ; i++) {

        String vId = null;

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youtubeModels.get(i).getVideoLink());

        if(matcher.find()){
            vId= matcher.group();
        }
        youtubeModels.get(i).setVideoId(vId);
        }
        VideoAdapter videoAdapter = new VideoAdapter(view.getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
        videoRecyclerView.setLayoutManager(layoutManager);
        videoRecyclerView.setItemAnimator(new DefaultItemAnimator());
        videoRecyclerView.setAdapter(videoAdapter);
        videoAdapter.setVideoList(youtubeModels);
        videoAdapter.setOnClick(new VideoAdapter.OnClickVideo() {
            @Override
            public void onClick(String videoLink) {
                view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videoLink)));

            }
        });
        // todo playing video form youtube.
//                    Log.i("Video", "Video Playing....");
        return view;
    }
}