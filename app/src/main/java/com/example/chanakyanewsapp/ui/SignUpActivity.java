package com.example.chanakyanewsapp.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.chanakyanewsapp.MainActivity;
import com.example.chanakyanewsapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends BaseAppCompatActivity {
    @BindView(R.id.buttonCreateAccount)
    AppCompatButton buttonCreateAccount;

    @BindView(R.id.textViewLoginText)
    AppCompatTextView textViewLoginText;

    @OnClick(R.id.buttonCreateAccount)
    void signUp(){
        MainActivity.startSelf(this);
    }

    @OnClick(R.id.textViewLoginText)
    void hadAnAccount(){
        MainActivity.startSelf(this);
    }

    @BindView(R.id.editTextName)
    AppCompatEditText editTextName;

    @BindView(R.id.editTextEmail)
    AppCompatEditText editTextEmail;

    @BindView(R.id.editTextPhone)
    AppCompatEditText editTextPhone;

    @BindView(R.id.editTextPassword)
    AppCompatEditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
    }
    public static void startSelf(Activity activity) {

        Intent intent = new Intent(activity, SignUpActivity.class);
        activity.startActivity(intent);
    }
}