package com.example.chanakyanewsapp.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import com.example.chanakyanewsapp.App;
import com.example.chanakyanewsapp.MainActivity;
import com.example.chanakyanewsapp.R;
import com.example.chanakyanewsapp.event.Events;
import com.example.chanakyanewsapp.utils.DialogHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

@SuppressWarnings("FieldCanBeLocal")
public class BaseAppCompatActivity extends AppCompatActivity {
    private static String TAG = "BaseAppCompatActivity";
    private static BaseAppCompatActivity instance;

    public Toolbar toolbar;
    View.OnClickListener backListener = view -> onBackPressed();
    private ViewGroup contentRootView;
    private View activityRootView;
    private ProgressDialog progressDialog;
    private AppCompatImageView btnBack;
    private AppCompatTextView actionBarTitle;
    private Dialog animatedLoaderDialog = null;
    private ProgressBar progressBarBackground;

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null) {
            if (inputManager != null) {
                inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    public static BaseAppCompatActivity getInstance() {
        return instance;
    }

    protected void setActionBarTitle(String title) {
        actionBarTitle.setText(title);
    }

    @SuppressWarnings("unused")
    protected void setActionBarElevation(float elevationValue) {
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getSupportActionBar().setElevation(elevationValue);
            }
        } else {
            Log.e(TAG, "setActionBarElevation: can not find supportActionBar");
        }
    }

    protected boolean needEventBus() {
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.base_app_compat_activity_layout);
        instance = this;
        initActionBar();
        contentRootView = findViewById(R.id.base_activity_content_layout);
        updateActionBarVisibility();
        initLoaders();
    }

    private void initLoaders() {
        initLoader();
        initBackgroundLoader();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (needEventBus()) {
            App.get().getEventBus().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (needEventBus()) {
            App.get().getEventBus().unregister(this);
        }
    }

    private void updateActionBarVisibility() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (toolbarNeeded()) {
                actionBar.show();
            } else {
                actionBar.hide();
            }
        }
    }

    protected boolean toolbarNeeded() {
        return false;
    }

    private void initActionBar() {
        toolbar = findViewById(R.id.toolbar);
        btnBack = toolbar.findViewById(R.id.btnBack);
        actionBarTitle = toolbar.findViewById(R.id.actionBarTitle);
        btnBack.setOnClickListener(backListener);
//        setActionBarElevation(.8f);
    }

    public void hideBackButton() {
        btnBack.setVisibility(View.GONE);
    }

    @Override
    public final void setContentView(@LayoutRes int layoutResID) {
        if (shouldOverrideRootView()) { //Override default method and add child layout in content container layout.
            activityRootView = getLayoutInflater().inflate(layoutResID, contentRootView, true); //last param must be true to add it as child.
        } else {
            super.setContentView(layoutResID);
            activityRootView = contentRootView.getChildAt(0);
        }
    }

    protected boolean shouldOverrideRootView() {
        return true;
    }

    public final void showShortToast(String message) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    public final void showLongToast(String message) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
    }

    protected void showToastFromUIThread(final String message) {
        runOnUiThread(() -> BaseAppCompatActivity.this.showShortToast(message));
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void userUnauthorizedEvent(Events.UserUnauthorizedEvent event) {
        hideLoader();
        event.removeSelf();
        showShortToast(getString(R.string.unauthorized_access_msg));
            App.get().getUserDataPreference(BaseAppCompatActivity.this).clearData();
            Intent intent = new Intent(BaseAppCompatActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
    }


    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void ServiceMaintenanceEvent(Events.ServiceMaintenanceEvent event) {
        hideLoader();
        event.removeSelf();
        DialogHelper.showDialogWithOkButton(this, "", getString(R.string.maintenance_msg), false, new DialogHelper.SingleButtonCallBack() {
            @Override
            public void onButtonClicked(Dialog dialog) {
                App.get().getUserDataPreference(BaseAppCompatActivity.this).clearData();
                Intent intent = new Intent(BaseAppCompatActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void ApiDownEvent(Events.ApiDownEvent event) {
        hideLoader();
        DialogHelper.showDialogWithOkButton(this, "", getString(R.string.api_down_msg), false, new DialogHelper.SingleButtonCallBack() {
            @Override
            public void onButtonClicked(Dialog dialog) {
                dialog.dismiss();
            }
        });
    }

    private void initLoader() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.progress_dialog_default_message));
    }

    public void hideLoader() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Throwable ignore) {
        }
    }

    private void initBackgroundLoader() {
        progressBarBackground = findViewById(R.id.progressBarBackground);
    }

    public void showBackgroundLoader() {
        progressDialog.show();
        progressDialog.setCancelable(false);
        if (progressBarBackground != null) {
            progressBarBackground.setVisibility(View.VISIBLE);
        }
    }

    public void hideBackgroundLoader() {
        progressDialog.dismiss();
        if (progressBarBackground != null) {
            progressBarBackground.setVisibility(View.GONE);
        }
    }

    public void hideBackgroundLoaderFromUIThread() {
        runOnUiThread(() -> BaseAppCompatActivity.this.hideBackgroundLoader());
    }



}
