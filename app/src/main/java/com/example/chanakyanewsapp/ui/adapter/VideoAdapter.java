package com.example.chanakyanewsapp.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.example.chanakyanewsapp.Constant;
import com.example.chanakyanewsapp.R;
import com.example.chanakyanewsapp.datastore.model.YoutubeModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {

    private Context context;
    private List<YoutubeModel> youtubeModels = new ArrayList<>();
    private OnClickVideo onClickVideo;
    public VideoAdapter(Context context) {
        this.context = context;

    }
    public void setVideoList(List<YoutubeModel> youtubeModels){
        this.youtubeModels = youtubeModels;
    }
    public void setOnClick(OnClickVideo onClick){
        this.onClickVideo = onClick;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_layout, parent, false);
        return new MyViewHolder(v);
    }



    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if(youtubeModels.size() > 0){
            Glide.with(context)
                    .load(Constant.thumbNailLink + youtubeModels.get(position).getVideoId()+Constant.defaultJpg)
                    .centerCrop()
                    .placeholder(R.drawable.image_placeholder)
                    .into(holder.videoImageView);

            holder.videImageCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    onClickVideo.onClick(youtubeModels.get(position).getVideoLink());
                }
            });
        }
    }




    @Override
    public int getItemCount() {
        return youtubeModels.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.videoImageView)
        AppCompatImageView videoImageView;

        @BindView(R.id.videImageCardView)
        CardView videImageCardView;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public interface OnClickVideo{
        void onClick(String videoLink);
    }
}