package com.example.chanakyanewsapp.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.chanakyanewsapp.R;
import com.example.chanakyanewsapp.ui.adapter.ImageAdapter;
import com.example.chanakyanewsapp.ui.adapter.NewsAdapter;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Images extends Fragment {

//    @BindView(R.id.imageRecyclerView)
//    RecyclerView imageRecyclerView;
    @BindView(R.id.cardViewStack)
    CardStackView cardStackView;
    public Images() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_images, container, false);
        ButterKnife.bind(this, view);
        ImageAdapter imageAdapter = new ImageAdapter(view.getContext());
        CardStackLayoutManager layoutManager = new CardStackLayoutManager(view.getContext());
        cardStackView.setLayoutManager(layoutManager);
        cardStackView.setItemAnimator(new DefaultItemAnimator());
        cardStackView.setAdapter(imageAdapter);
        cardStackView.swipe();
        return view;
    }
}