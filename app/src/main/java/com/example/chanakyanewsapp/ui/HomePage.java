package com.example.chanakyanewsapp.ui;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.example.chanakyanewsapp.R;
import com.example.chanakyanewsapp.ui.adapter.ViewPagerAdapter;
import com.example.chanakyanewsapp.ui.fragments.Images;
import com.example.chanakyanewsapp.ui.fragments.NewsFragment;
import com.example.chanakyanewsapp.ui.fragments.UserProfile;
import com.example.chanakyanewsapp.ui.fragments.VideoFragment;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomePage extends BaseAppCompatActivity {
    @BindView(R.id.tabLayout)
    TabLayout bottom_navigation;

    @BindView(R.id.itineraryViewPager)
    ViewPager itineraryViewPager;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        ButterKnife.bind(this);
        init();
        setupTabIcons();
    }

    private void init() {
        bottom_navigation.setupWithViewPager(itineraryViewPager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new NewsFragment(),"Home");
        adapter.addFragment(new Images(), "Images");
        adapter.addFragment(new VideoFragment(), "Videos");
        adapter.addFragment(new UserProfile(), "Profile ");
        bottom_navigation.setTabTextColors(ContextCompat.getColor(this, R.color.input_text_color), ContextCompat.getColor(this, R.color.text_epoch));
        itineraryViewPager.setAdapter(adapter);
        itineraryViewPager.setCurrentItem(0);

    }
    private void setupTabIcons() {
        bottom_navigation.getTabAt(0).setIcon(R.drawable.home).setText("Home");
        bottom_navigation.getTabAt(1).setIcon(R.drawable.image).setText("Images");
        bottom_navigation.getTabAt(2).setIcon(R.drawable.video).setText("Videos");
        bottom_navigation.getTabAt(3).setIcon(R.drawable.profile).setText("Profile");
    }

    public static void startSelf(Activity activity) {
        Intent intent = new Intent(activity, HomePage.class);
        activity.startActivity(intent);
    }
}