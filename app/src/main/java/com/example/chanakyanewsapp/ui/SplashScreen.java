package com.example.chanakyanewsapp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.example.chanakyanewsapp.MainActivity;
import com.example.chanakyanewsapp.R;

public class SplashScreen extends BaseAppCompatActivity {
   private final int SPLASH_TIME_OUT = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                MainActivity.startSelf(SplashScreen.this);
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}