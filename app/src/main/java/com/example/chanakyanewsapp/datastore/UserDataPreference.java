package com.example.chanakyanewsapp.datastore;

import android.content.Context;
import android.content.SharedPreferences;

public class UserDataPreference {

    private static final String PREFERENCE_NAME = "user_data_preference";
    private final String USER_ID = "user_id";
    private final String EMAIL = "email";
    private final String FULL_NAME = "full_name";
    private final String FCM_TOKEN="fcm_token";
    private final String MOBILE="mobile";
    private final SharedPreferences sp;

    public UserDataPreference(Context context) {
        sp = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }
    public void clearData() {
        sp.edit().clear().apply();
    }

    public String getEmail() {
        return sp.getString(EMAIL, "");
    }


    public String getFullName() {
        return sp.getString(FULL_NAME, "");
    }

    public void setEmail(String value) {
        sp.edit().putString(EMAIL, value).apply();
    }

    public String getUserId() {
        return sp.getString(USER_ID, "");
    }

    public void setUserId(String value) {
        sp.edit().putString(USER_ID, value).apply();
    }

    public void setFullName(String value) {
        sp.edit().putString(FULL_NAME, value).apply();
    }

    public String getFcmToken() {
        return sp.getString(FCM_TOKEN, "");
    }

    public void setFcmToken(String value) {
        sp.edit().putString(FCM_TOKEN, value).apply();
    }

    public String getMobile() {
        return sp.getString(MOBILE, "");
    }

    public void setMobile(String value) {
        sp.edit().putString(MOBILE, value).apply();
    }


}
