package com.example.chanakyanewsapp.datastore.model;

public class YoutubeModel {
    private String videoLink;
    private String videoId;

    public YoutubeModel(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
