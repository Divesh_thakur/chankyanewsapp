package com.example.chanakyanewsapp;

public class Constant {
    public static final String videoId1="https://www.youtube.com/watch?v=b6xK4VA-cw0&t=4s";
    public static final String videoId2="https://www.youtube.com/watch?v=h6rs4s-pAg4";
    public static final String videoId3="https://www.youtube.com/watch?v=7aWFqEapOJI";
    public static final String videoId4="https://www.youtube.com/watch?v=23mW-W3EZBc";
    public static final String videoId5="https://www.youtube.com/watch?v=EuRtL-Q-J1c";
    public static final String videoId6="https://www.youtube.com/watch?v=zB3AdpRh0yQ";
    public static final String videoId7="https://www.youtube.com/watch?v=joC-HpxxiTI";
    public static final String videoId8="https://www.youtube.com/watch?v=BrvB3vrRwYw";
    public static final String videoId9="https://www.youtube.com/watch?v=soLvhwTUXv8";
    public static final String thumbNailLink = "http://img.youtube.com/vi/";
    public static final String defaultJpg = "/default.jpg";

}
