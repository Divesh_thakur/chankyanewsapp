package com.example.chanakyanewsapp.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.chanakyanewsapp.R;


public class DialogHelper {
    private static final String TAG = "DialogHelper";

    public static void showDialogWithOkButton(Context context, String title, String msg, boolean isCancelable, SingleButtonCallBack callback) {
        if (context == null || (context instanceof Activity && ((Activity) context).isFinishing())) {
            Log.d(TAG, "showDialog invalid context, return");
            return;
        }

        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context).setCancelable(isCancelable);
            View view = LayoutInflater.from(context).inflate(R.layout.custom_alert_box, null);
            TextView textViewTitleSingleDialog = view.findViewById(R.id.textViewTitleSingleDialog);
            TextView textViewMessageSingleDialog = view.findViewById(R.id.textViewMessageSingleDialog);
            alertDialogBuilder.setView(view);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            dismissDialogOnBackPressed(alertDialog);
            textViewTitleSingleDialog.setText(title);
            textViewMessageSingleDialog.setText(msg);
            view.findViewById(R.id.buttonOk).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callback != null) {
                        callback.onButtonClicked(alertDialog);
                    }
                }
            });
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        } catch (Throwable ignore) {
        }
    }

    public static void showInformativeDialog(final Context context, String title, String message,
                                             String positiveButtonText, String negativeButtonText, boolean cancellable,
                                             boolean isTitleCaps, final TwoOptionCallBack callback) {

        if (context == null || (context instanceof Activity && ((Activity) context).isFinishing())) {
            Log.d(TAG, "showDialog invalid context, return");
            return;
        }
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context).setCancelable(cancellable);

            View view = LayoutInflater.from(context).inflate(R.layout.custom_postive_negative_button_alert, null);
            alertDialogBuilder.setView(view);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            dismissDialogOnBackPressed(alertDialog);
            TextView textViewAlertTitle=view.findViewById(R.id.textViewAlertTitle);
            TextView textViewAlertMessage=view.findViewById(R.id.textViewAlertMessage);
            Button buttonPositive=view.findViewById(R.id.buttonPositive);
            Button buttonNegative=view.findViewById(R.id.buttonNegative);

            if (!TextUtils.isEmpty(title)) {
                textViewAlertTitle.setText(title);
            }

            if (!TextUtils.isEmpty(message)) {
                textViewAlertMessage.setText(message);
            }

            if (!TextUtils.isEmpty(positiveButtonText)) {
                buttonPositive.setText(positiveButtonText);
            }
            if (!TextUtils.isEmpty(negativeButtonText)) {
                buttonNegative.setText(negativeButtonText);
            }

            textViewAlertTitle.setAllCaps(isTitleCaps);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss(); //first dismiss it, else on activity finish we may get leak.
                    if (callback != null) {
                        if (view.getId() == R.id.buttonPositive) {
                            callback.onPositiveAction(alertDialog);
                        } else if (view.getId() == R.id.buttonNegative) {
                            callback.onNegativeAction(alertDialog);
                        }
                    }
                }
            };

            buttonPositive.setOnClickListener(clickListener);
            buttonNegative.setOnClickListener(clickListener);
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }

        } catch (Throwable ignore) {
        }
    }

    private static void dismissDialogOnBackPressed(AlertDialog alertDialog) {
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
    }



    public interface SingleButtonCallBack {
        void onButtonClicked(Dialog dialog);
    }
    public interface TwoOptionCallBack {
        void onPositiveAction(Dialog dialog);
        void onNegativeAction(Dialog dialog);
    }
}
