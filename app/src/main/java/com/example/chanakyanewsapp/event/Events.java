package com.example.chanakyanewsapp.event;


import com.example.chanakyanewsapp.App;

public class Events {

    public static class BaseStickyEvent {
        public void removeSelf() {
            App.get().getEventBus().removeStickyEvent(this);
        }
    }

    public static class BaseRestStickyEvent extends BaseStickyEvent {
        private ErrorModel errorModel;

        BaseRestStickyEvent(ErrorModel errorModel) {
            this.errorModel = errorModel;
        }

        public ErrorModel getErrorModel() {
            return errorModel;
        }

        public boolean isSessionInvalid() {
            return errorModel.getErrorCode().equals("401");
        }

        public String getErrorMessage() {
            return errorModel.getErrorMsg();
        }

        public boolean isSuccess() {
            return errorModel == null;
        }
    }


    public static class BaseRestEvent {
        private ErrorModel errorModel;

        BaseRestEvent(ErrorModel errorModel) {
            this.errorModel = errorModel;
        }

        public ErrorModel getErrorModel() {
            return errorModel;
        }

        public String getErrorMessage() {
            return errorModel.getErrorMsg();
        }

        public boolean isSuccess() {
            return errorModel == null;
        }
    }

    public static class UserUnauthorizedEvent extends BaseStickyEvent {
    }

    public static class ServiceMaintenanceEvent extends BaseStickyEvent {
    }

    public static class ApiDownEvent extends BaseStickyEvent {
    }

    public static class SignUpEvent extends BaseRestStickyEvent {
        private final String message;

        public SignUpEvent(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class LoginEvent extends BaseRestStickyEvent{
        private final String message;

        public LoginEvent(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
    public static class ResetPasswordEvent extends BaseRestStickyEvent{
        private final String message;

        public ResetPasswordEvent(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class UserProfileEvent extends BaseRestStickyEvent{
        private final String message;

        public UserProfileEvent(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
    public static class SeatGeekSuggestionLoaded extends BaseRestStickyEvent{
        private final String message;

        public SeatGeekSuggestionLoaded(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class NpsSuggestionLoaded extends BaseRestStickyEvent{
        private final String message;

        public NpsSuggestionLoaded(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
    public static class UpcomingEventsLoaded extends BaseRestStickyEvent{
        private final String message;

        public UpcomingEventsLoaded(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
    public static class PreviousEventsLoaded extends BaseRestStickyEvent{
        private final String message;

        public PreviousEventsLoaded(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
    public static class ManualItineraryRequest extends BaseRestStickyEvent{
        private final String message;

        public ManualItineraryRequest(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class EventVenueSuggestion extends BaseRestStickyEvent{
        private final String message;

        public EventVenueSuggestion(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
    public static class FcmToken extends BaseRestStickyEvent{
        private final String message;

        public FcmToken(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class DeclineAdminSuggestionEvent extends BaseRestStickyEvent{
        private final String message;

        public DeclineAdminSuggestionEvent(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class BookingConfirmationEvent extends BaseRestStickyEvent{
        private final String message;

        public BookingConfirmationEvent(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class ManualReqSuggestionsEvent extends BaseRestStickyEvent {
        private final String message;

        public ManualReqSuggestionsEvent(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
    public static class CancelManualRequestEvent extends BaseRestStickyEvent{
        private final String message;

        public CancelManualRequestEvent(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class SendItineraryToMail extends BaseRestStickyEvent{
        private final String message;

        public SendItineraryToMail(String message, ErrorModel errorModel) {
            super(errorModel);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}
