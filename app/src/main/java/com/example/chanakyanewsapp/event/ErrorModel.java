package com.example.chanakyanewsapp.event;



public class ErrorModel {
    public static final String DEFAULT_REST_ERROR_CODE = "default_rest_error_code";

    private final String errorCode;
    private final String errorMsg;

    public ErrorModel(String errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

//    public static ErrorModel defaultUnknownError() {
//        if (!GenericUtils.isInternetOn()) {
//            return new ErrorModel(Constants.DEFAULT_REST_NO_NET_ERROR_CODE, App.get().getString(R.string.no_internet_error));
//        } else {
//            return new ErrorModel(Constants.DEFAULT_REST_ERROR_CODE, App.get().getString(R.string.error_in_connection));
//        }
//    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public String toString() {
        return "ErrorView{" +
                "Code='" + errorCode + '\'' +
                ", Msg='" + errorMsg + '\'' +
                '}';
    }
}
